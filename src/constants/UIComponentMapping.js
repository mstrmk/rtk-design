import Button from '@/components/UIComponents/Button/Button';
import ButtonProps from '@/components/UIComponents/Button/ButtonProps';
import Input from '@/components/UIComponents/Input/Input';
import InputProps from '@/components/UIComponents/Input/InputProps';
import CheckBox from '@/components/UIComponents/CheckBox/CheckBox';
import CheckBoxProps from '@/components/UIComponents/CheckBox/CheckBoxProps';
import Select from '@/components/UIComponents/Select/Select';
import SelectProps from '@/components/UIComponents/Select/SelectProps';
import Textarea from '@/components/UIComponents/Textarea/Textarea';
import TextareaProps from '@/components/UIComponents/Textarea/TextareaProps';

export const elements = {
    'button': [Button, ButtonProps],
    'input': [Input, InputProps],
    'checkbox': [CheckBox, CheckBoxProps],
    'select': [Select, SelectProps],
    'textarea': [Textarea, TextareaProps]
};

export const getComponentByName = (cmpName) => {
    return elements[cmpName] ? elements[cmpName][0] : null;
};

export const getComponentPropsFormByName = (cmpName) => {
    return elements[cmpName] ? elements[cmpName][1] : null;
};